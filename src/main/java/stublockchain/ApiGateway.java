package stublockchain;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApiGateway {
	
	private ImageInscripcion blockchain;
	
	
	public JSONArray getInscripciones (String str) throws IOException {
		JSONArray inscripciones = blockchain.getInscripciones();
		return inscripciones;
		
	}
	
	public void insertInscripcion (JSONObject objeto) throws IOException {
		blockchain.insertInscripcion(objeto);
	}
	
}
