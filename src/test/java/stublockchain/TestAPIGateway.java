package stublockchain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.json.JSONArray;
import org.junit.Test;


public class TestAPIGateway {
	
	@Test
	public void testGetInscripciones() throws Exception {
		ApiGateway gateway = new ApiGateway();
		JSONArray InscripcionesApi=gateway.getInscripciones("");
		assertEquals(InscripcionesApi.length(),1);
	}
}
